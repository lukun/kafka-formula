#!/bin/bash

# Licensed to the Apache Software Foundation (ASF) under one or more
# contributor license agreements.  See the NOTICE file distributed with
# this work for additional information regarding copyright ownership.
# The ASF licenses this file to You under the Apache License, Version 2.0
# (the "License"); you may not use this file except in compliance with
# the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

#
# Kafka
#
# chkconfig: 2345 89 9
# description: kafka

source /etc/rc.d/init.d/functions

START_SERVER_SCRIPT={{ workdir }}/bin/kafka-server-start.sh
STOP_SERVER_SCRIPT={{ workdir }}/bin/kafka-server-stop.sh
SERVER_CONFIG_PATH={{ workdir }}/config/server.properties
RETVAL=0
PIDFILE="/var/run/kafka/kafka-server.pid"
desc="Kafka broker"

start() {
  echo -n $"Starting $desc (kafka): "
  daemon --user kafka ${START_SERVER_SCRIPT} -daemon ${SERVER_CONFIG_PATH}
  RETVAL=$?
  echo `ps ax | grep -i 'kafka\.Kafka' | grep java | grep -v grep | awk '{print $1}'` > ${PIDFILE}
  echo
  [ $RETVAL -eq 0 ] && touch /var/lock/subsys/kafka
  return $RETVAL
}

stop() {
  echo -n $"Stopping $desc (kakfa): "
  daemon --user kafka ${STOP_SERVER_SCRIPT}
  RETVAL=$?
  sleep 5
  echo
  [ $RETVAL -eq 0 ] && rm -f /var/lock/subsys/kafka $PIDFILE
}

restart() {
  stop
  start
}

checkstatus(){
  status -p $PIDFILE kafka
  RETVAL=$?
}

condrestart(){
  [ -e /var/lock/subsys/kafka ] && restart || :
}

case "$1" in
  start)
    start
    ;;
  stop)
    stop
    ;;
  status)
    checkstatus
    ;;
  restart)
    restart
    ;;
  condrestart)
    condrestart
    ;;
  *)
    echo $"Usage: $0 {start|stop|status|restart|condrestart}"
    exit 1
esac

exit $RETVAL
