{%- from 'kafka/settings.sls' import kafka, config with context %}

include:
  - kafka

kafka-server-conf:
  file.managed:
    - name: {{ kafka.real_home }}/config/server.properties
    - source: salt://kafka/config/server.properties
    - user: kafka
    - group: kafka
    - mode: 644
    - template: jinja
    - require:
      - cmd: install-kafka-dist

/etc/init.d/kafka:
  file.managed:
    - source: salt://kafka/config/kafka.init.d
    - mode: 755
    - template: jinja
    - context:
      workdir: {{ kafka.prefix }}
    - require:
      - file: kafka-server-conf

kafka-service:
  service.running:
    - name: kafka
    - enable: true
    - watch:
      - file: kafka-server-conf
      - file: /etc/init.d/kafka
